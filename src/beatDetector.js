
// fork getUserMedia for multiple browser versions, for those
// that need prefixes

navigator.getUserMedia = (navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia);

// set up forked web audio context, for multiple browsers
// window. is needed otherwise Safari explodes
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

var BeatDetector = oo.createClass({
    _distortion:   null,
    _gainNode:     null,
    _biquadFilter: null,
    _convolver:    null,

    _timeDomain:   null,
    _freqDomain:   null,
    _freqHistory:  null,

    _initalized:   false,
    _bufferSize:   2048,

    getTimeDomain:      function getTimeDomain()      { return this._timeDomain; },
    getFrequencyDomain: function getFrequencyDomain() { return this._freqDomain; },
    getFrequencyHist:   function getFrequencyHist()   { return this._freqHistory; },

    update: function update() {
        if (this._initalized) {
            this._analyser.getByteTimeDomainData(this._timeDomain);
            this._analyser.getByteFrequencyData(this._freqDomain);
            var j, count2, freq0, freq1;
            for(var i=this._freqHistory.length-1; i>0; --i) {
                freq0 = this._freqHistory[i-1];
                freq1 = this._freqHistory[i];

                for(j=0, count2 = freq0.length; j<count2; ++j) {
                    freq1[j] = freq0[j];
                }
            }

            freq0 = this._freqHistory[0];
            for(j=0, count2 = freq0.length; j<count2; ++j) {
                freq0[j] = this._freqDomain[j];
            }

            //@TODO process here
        }
    },

    _create: function _create(bufferSize) {
        if (bufferSize) {
            this._bufferSize = bufferSize;
        }


        this._analyser = audioCtx.createAnalyser();
        this._analyser.minDecibels = -90;
        this._analyser.maxDecibels = -10;
        //this._analyser.smoothingTimeConstant = 0.85;
        //this._analyser.smoothingTimeConstant = 0.95;
        this._analyser.smoothingTimeConstant = 0.5;
        this._timeDomain = new Uint8Array(this._analyser.fftSize);
        this._freqDomain = new Uint8Array(this._analyser.frequencyBinCount);
        this._freqHistory = [];
        for(var i=0; i<32; ++i) {
            this._freqHistory.push(new Uint8Array(this._analyser.frequencyBinCount));
        }


        if (navigator.getUserMedia) {
           console.log('getUserMedia supported.');
           navigator.getUserMedia (
              // constraints - only audio needed for this app
              {
                audio: true
              },

              // Success callback
              function(stream) {
                 this._source = audioCtx.createMediaStreamSource(stream);
                 this._source.connect(this._analyser);

                this._analyser.fftSize = this._bufferSize;
                
                this._initalized = true;
                console.log("BeatDetector initalized with fft size " + this._analyser.fftSize);

              }.bind(this),

              // Error callback
              function(err) {
                 console.log('The following gUM error occured: ' + err);
              }
           );
        } else {
           console.log('getUserMedia not supported on your browser!');
        }
    },


});