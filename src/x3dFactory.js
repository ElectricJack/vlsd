var X3DElement = oo.createClass({
    vec3Str:        function vec3Str(v)       { return v[0]+","+v[1]+","+v[2]; },
    vec4Str:        function vec4Str(v)       { return v[0]+","+v[1]+","+v[2]+","+v[3]; },
    color3Str:      function color3Str(c)     { return c[0]+" "+c[1]+" "+c[2]; },
});

var X3DObject = X3DElement({
    _xform:         null,
    _parentElement: null,

    // properties:
    _pos:  null,
    _size: null,
    _rot:  null,

    _create: function _create(builder, id, pos, size, rot) {
        this._pos         = pos;
        this._size        = size || [1,1,1];
        this._rot         = rot  || [0,0,0,0];
        this._xform       = builder.createTransform(id, pos, size, rot);
        //console.log("X3DObject._create " + this._pos + " s: "+this._size+" r: " + this._rot);
    },


    getRootElement: function getRootElement() { return this._xform; },

    attach: function attach(parent) {
        //this._parentElement
        if (typeof parent['getRootElement'] === 'function') {
            this._parentElement = parent.getRootElement();
        } else {
            this._parentElement = parent;
        }

        if (this._parentElement) {
            this._parentElement.appendChild(this._xform);
        }

        return this;
    },



    getPosition: function getPosition() { return this._pos; },
    setPosition: function setPosition(pos) {
        this._pos = pos;
        this._xform.setAttribute("translation", this.vec3Str(this._pos));
    },

    getSize: function getSize() { return this._size; },
    setSize: function setSize(size) {
        this._size = size;
        this._xform.setAttribute("scale", this.vec3Str(this._size));
    },

    getRotation: function getRotation() { return this._rot; },
    setRotation: function setRotation(rot) {
        this._rot = rot;
        this._xform.setAttribute("rotation", this.vec4Str(this._rot));
    },

});

var X3DBox = X3DObject({
    // DOM elements:
    _appearance:    null,
    _material:      null,
    _shape:         null,
    _box:           null,


    _create: function _create(builder, id, pos, size, rot, mat) {
        //console.log("X3DBox._create " + pos + " s: "+size+" r: " + rot);
        this._super('_create', builder, id, pos, size, rot);

        //this._appearance = builder.createNode('material', builder._actives.material);
        this._appearance = mat.cloneNode(true);
        this._shape      = builder.createNode('shape', builder._actives.shape);

        this._box = builder.createNode(id+'_box', {
            size:            "1,1,1",
            render:          "true",
            ccw:             'true',
            //hasHelperColors: 'false',
            //lit:             'true',
            //metadata:        'X3DMetadataObject',
            solid:           'false',
            //useGeoCache:     'false',
        });

        this._shape.appendChild(this._appearance);
        this._shape.appendChild(this._box);
        this._xform.appendChild(this._shape);
    },
});

var X3DSphere = X3DObject({
    _appearance:    null,
    _shape:         null,
    _sphere:        null,

    _subdivision:   '24,24',

    _create: function _create(builder, id, pos, size, rot, subdivision, mat) {
        this._super('_create', builder, id, pos, size, rot);
        this._subdivision = subdivision || '24,24';

        this._appearance  = mat.cloneNode(true);
        this._shape       = builder.createNode('shape', builder._actives.shape);

        this._sphere = builder.createNode('sphere', {
            bboxCenter:       "0,0,0",
            bboxSize:         "-1,-1,-1",
            subdivision:      this._subdivision,
            radius:           "1",
            center:           "0,0,0",
            render:           "true"
        });

        this._shape.appendChild(this._appearance);
        this._shape.appendChild(this._sphere);
        this._xform.appendChild(this._shape);
    },


});

var X3DModel = X3DObject({
    // DOM elements:
    _appearance:    null,
    _material:      null,
    _inline:        null,


    _create: function _create(builder, id, url, pos, size, rot) {
        this._super('_create', builder, id, pos, size, rot);
        // this._pos        = pos;
        // this._size       = size;
        this._builder    = builder;
        this._appearance = builder.createNode('appearance', builder._actives.appearance);
        this._material   = builder.createNode('material', builder._actives.material);

        this._inline = builder.createNode('inline', {
            bboxCenter:       "0,0,0",
            bboxSize:         "-1,-1,-1",
            mapDEFToIDkey:   "true",
            url:              url,
            render:          "true"
        });

        this._appearance.appendChild(this._material);
        this._inline.appendChild(this._appearance);
        this._xform.appendChild(this._inline);
    },
});

var X3DPointLight = X3DObject({
    // DOM elements:
    _light:        null,


    _create: function _create(builder, id, pos) {
        this._super('_create', builder, id, pos);

        this._light = builder.createNode('PointLight', {
            ambientIntensity:'0',
            attenuation:'1,0,0',
            color:'1,1,1',
            global:'false',
            intensity:'6',
            location: '0,0,0',
            radius:'100',
            shadowFilterSize:'0',
            shadowIntensity:'0.8',
            shadowMapSize:'1024',
            shadowOffset:'0.01',
            zFar:'-1',
            zNear:'-1'
        });

        this._xform.appendChild(this._light);
    },
});



var X3DFactory = X3DElement({
    _loggingEnabled: false,
    _defaults:       null,
    _actives:        null,

    _create: function _create() {
        this._initDefaults();
        this._initActives();
    },

    createNode: function createNode(nodeName, attributes) {
        this._log("Creating node " + nodeName);
        var node = document.createElement(nodeName);

        _(attributes).forEach(function(value, name) {
            this._log("Setting attribute " + name + ": '" + value + "'");
            node.setAttribute(name, value);
        }.bind(this));
        return node;
    },

    createBox:        function createBox(id, pos, size, rot, mat)                 { return new X3DBox(this, id, pos, size, rot, mat); },
    createSphere:     function createSphere(id, pos, size, rot, subdivision, mat) { return new X3DSphere(this, id, pos, size, rot, subdivision, mat); },
    createModel:      function createModel(id, url, pos, size, rot)               { return new X3DModel(this, id, url, pos, size, rot); },
    createPointLight: function createPointLight(id, pos)                          { return new X3DPointLight(this, id, pos); },

    createMaterial: function createMaterial(diffColor, specColor, shiny, ambIntensity, emColor, imgUrl, movieUrl) {
        
        var mat = 
        {
            diffuseColor:     diffColor,
            specularColor:    specColor,
            shininess:        shiny,
            ambientIntensity: ambIntensity,
            emissiveColor:    emColor 
        };
        //console.log("X3DObject.createTransform p: " + pos + " s: "+size+" r: " + rot);
        var appNode = this.createNode('appearance', this._actives.appearance);
        var matNode = this.createNode('material', mat);
        appNode.appendChild(matNode);

        if(imgUrl !== undefined)
        {
            var imgTex = 
            {
                repeatS:     'false',
                repeatT:    'false',
                url:        imgUrl
            };
            var imgNode = this.createNode('ImageTexture', imgTex);
            appNode.appendChild(imgNode);
        }
            
        if(movieUrl !== undefined)
        {
            var movTex = 
            {
                repeatS:     'false',
                repeatT:    'false',
                loop:       "true",
                url:        movieUrl
            };
            var movNode = this.createNode('MovieTexture', movTex);
            appNode.appendChild(movNode);
        }

        return appNode;
    },

    createTransform: function createTransform(id, pos, size, rot) {
        rot  = rot  || [0,0,0,0];
        size = size || [1,1,1];
        //console.log("X3DObject.createTransform p: " + pos + " s: "+size+" r: " + rot);
        return this.createNode('transform', {
            DEF:              id,
            render:           "true",
            bboxCenter:       "0,0,0",
            bboxSize:         "-1,-1,-1",
            center:           "0,0,0",
            translation:      this.vec3Str(pos),
            rotation:         this.vec4Str(rot),
            scale:            this.vec3Str(size),
            scaleOrientation: "0,0,0,0"
        });
    },
        
    // ---------------------------------------------------------------------------------------- //
    // Private impl.

    _initDefaults: function _initDefaults() {
        this._defaults = {};
        this._defaults.appearance = {
            sortType: "auto"
        };
        this._defaults.material = {
            diffuseColor:     "1 1 1",
            specularColor:    "0.000 0.000 0.000",
            shininess:        "0.145",
            ambientIntensity: "0.05",
            emissiveColor:    "0,0,0"
        };
        this._defaults.shape = {
            render:     "true",
            bboxCenter: "0,0,0",
            bboxSize:   "-1,-1,-1",
            isPickable: "true"
        };
    },
    _initActives: function _initActives() {
        this._actives = {};
        _(this._defaults).forEach(function(value, name) {
            this._actives[name] = _(value).clone();
        }.bind(this));
    },
    _log: function log(str) {
        if (this._loggingEnabled) {
            console.log(str);
        }
    },
});