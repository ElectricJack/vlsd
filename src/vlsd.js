


// Global materials
var basicMat;
var sunMat;
var worldMats = [];


//console.log("Running..");
window.onload = function() {
    var builder   = new X3DFactory();
    var detector  = new BeatDetector(1024);
    var root = document.getElementById('rootDef');

    basicMat = builder.createMaterial("0.588 0.588 0.588", "0.000 0.000 0.000", "0.145", "0.2", "0,0,0");

    var textures = [
        '"img/planet01.jpg"',
        '"img/planet02.png"',
        '"img/planet03.jpg"'
    ];
        
    _(textures).forEach(function(texture) {
        var mat = builder.createMaterial("0.588 0.588 0.588", "0.000 0.000 0.000", "0.145", "0.2", "0,0,0");
        var tex = builder.createNode('ImageTexture', {
            repeatS: 'true',
            repeatT: 'true',
            url:     texture
        });
        mat.appendChild(tex);
        worldMats.push(mat);
    }.bind(this));


    sunMat = builder.createMaterial("0.9 0.9 0.9", "0.000 0.000 0.000", "0.0", "0.0", "0.8,0.5,0.5");
    var tex = builder.createNode('ImageTexture', {
        repeatS: 'true',
        repeatT: 'true',
        url:     '"img/sunTex.png"'
    });
    sunMat.appendChild(tex);

    tex = builder.createNode('ImageTexture', {
        repeatS: 'true',
        repeatT: 'true',
        url:     '"img/sunTex.png"'
    });
    basicMat.appendChild(tex);


    testGenerator(root, builder, detector);
    
    

    //testFFT2(root, builder, detector);

};
