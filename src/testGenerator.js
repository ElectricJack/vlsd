

var Orbiter = X3DObject({
    world:    null,
    //_children: null,
    //_angVel:   0.01,
    getTilt: function getTilt() { return Math.random() * 0.3 - 0.15; },

    _create: function _create(builder, id, pos, size, rad) {
        this._super('_create', builder, id, pos, [1,1,1]);
        //this._shape = builder.createSphere(id,pos,size,rot).attach(this);

        //this._axis  = []
        var TWOPI = 3.14159 * 2;
        this.orbitPhase      = Math.random() * TWOPI;
        this.rotatePhase     = Math.random() * TWOPI;
        this.worldOrbitRad   = rad;
        this.worldAngVel     = Math.random()*0.5 - 0.25;
        this.worldOrbitVel   = 20.0 / (this.worldOrbitRad*this.worldOrbitRad);
        this.worldRotateAxis = [this.getTilt(),0.8,this.getTilt(),0];
        this.worldOrbitAxis  = [this.getTilt(),0.8,this.getTilt(),0];
        
        
        var texIndex = Math.floor(Math.random()*100) % worldMats.length;
        this.world = builder.createSphere(id+"sph", [0,0,this.worldOrbitRad], size, [0,0,0,0], "24,24", worldMats[texIndex]).attach(this);
    },

    update: function update(time) {
        //this.setRotation()
        this.worldOrbitAxis[3] = this.orbitPhase + time * this.worldAngVel;
        this.worldRotateAxis[3] = this.rotatePhase + time * this.worldOrbitVel;
        this.setRotation(this.worldRotateAxis);
        this.world.setRotation(this.worldOrbitAxis);
    },
});



function testGenerator(root, builder, detector) {

    //var w0 = new World(builder, "w0", [0,0,-10], [10,10,10]);
    var sunOrigin  = [0,0,-60];
    var sunAngVel  = 0.1;
    var sunRotAxis = [0,0.8,0.2];

    var sunLight   = builder.createPointLight("sun", sunOrigin, [1,1,1]).attach(root);
    var sun        = builder.createSphere("sun", [0,0,0], [10,10,10], [0,0,0,0], '24,24', sunMat).attach(sunLight);


    var worlds = [];
    for(var i=0; i<15; ++i) {
        var rad = 0.05 + Math.random() * 3;
        var orbitRad = 15.0 + Math.random() * 35;
        var world  = new Orbiter(builder,"w" + i, [0,0,0], [rad,rad,rad], orbitRad);

        var moons = 2;//Math.floor(Math.random()*4);
        for(var m=0; m<moons; ++m) {
            var rad2 = 0.05 + Math.random();
            var orbitRad2 = 2.0 + Math.random() * 2;
            var moon  = new Orbiter(builder,"m" + i + "_" + m, [0,0,0], [rad2,rad2,rad2], orbitRad2);
            moon.attach(world);
            worlds.push(moon);
        }

        world.attach(sunLight);
        worlds.push(world);
    }

    var scale = 10;

    var boxWaveStart  = 1000 * scale;
    var boxWaveEnd    = 3000 * scale;

    var dragonStart   = 2000 * scale;
    var dragonEnd     = 4000 * scale;

    var testFFTStart  = 3000 * scale;
    var testFFTEnd    = 5000 * scale;

    var testFFT2Start = 4000 * scale;
    //var testFFT2End   = 10000* scale;

    var boxWaveRoot = null;
    var dragonRoot  = null;
    var fftRoot     = null;
    var fft2Root    = null;


    window.setTimeout(function() { boxWaveRoot = testBoxWave(sunLight, builder); }, boxWaveStart);
    window.setTimeout(function() { boxWaveRoot.parentNode.removeChild(boxWaveRoot); }, boxWaveEnd);

    window.setTimeout(function() { dragonRoot = startDragon(sunLight, builder, [0,0,40]); }, dragonStart);
    window.setTimeout(function() { dragonRoot.parentNode.removeChild(dragonRoot);}, dragonEnd);

    window.setTimeout(function() { fftRoot = testFFT(sunLight, builder, detector); }, testFFTStart);
    window.setTimeout(function() { fftRoot.parentNode.removeChild(fftRoot);}, testFFTEnd);

    window.setTimeout(function() { fft2Root = testFFT2(sunLight, builder, detector); }, testFFTStart);
    //window.setTimeout(function() { fft2Root.parentNode.removeChild(fft2Root);}, testFFT2End);

    
    var freqs = detector.getFrequencyDomain();

    var time = 0;
    window.setInterval( function() {
        time += 0.1;

        for(var i=0; i<worlds.length; ++i) {
            var world = worlds[i];
            world.update(time);
        }

    }, 10);
}