function testFFT(testScene, builder, detector) {
    var group = builder.createNode("group",{
        id: "fft"
    });
    testScene.getRootElement().appendChild(group);

    var boxes = [];
    var barCount = 128;
    for(var i=0; i<barCount; ++i) {
        var t = i / barCount;
        var x = lerp(t, -100, 100);
        var id = "box"+i;
        //boxes[i] = builder.createBox(id,[x, 0, -30], [1.5,1.5,1.5], [0,0,0,0], basicMat).attach(testScene);
        boxes[i] = builder.createSphere(id,[x, 0, -30], [1.5,1.5,1.5], [0,0,0,0], '3,2', basicMat).attach(root);
    }
   

    window.setInterval( function() {
        detector.update();
        var freqs = detector.getFrequencyDomain();
        //var freqs = detector.getTimeDomain();
        if (freqs) {
            for(var i=0, count = boxes.length; i<count; ++i) {
                var mappedIndex = Math.floor(map(i, 0,count, 0, freqs.length/4.0));
                var s = boxes[i].getSize();
                s[1] = freqs[mappedIndex];
                boxes[i].setSize(s);
            }
        }
    }, 10);

    return group;
}



function testFFT2(testScene, builder, detector) {
    var group = builder.createNode("group",{
        id: "fft2"
    });
    testScene.getRootElement().appendChild(group);

    var boxes = [];
    var barCount = 16;
    var histories = 16;

    var colorMats = [];
    for(var i=0; i<barCount; ++i) {
        var color = HSVtoRGB(map(i,0,barCount, 0,255),255,128);
        colorMats[i] = builder.createMaterial("1 1 1","0.5 0.5 0.5","0.125","0.2",color);
        tex = builder.createNode('ImageTexture', {
            repeatS: 'true',
            repeatT: 'true',
            url:     '"img/boxTex.png"'
        });
        colorMats[i].appendChild(tex);
    }
    

    for(var h=0; h<histories; ++h) {
        var ht = h / histories;
        var z = lerp(ht, -100, -400);
        for(var i=0; i<barCount; ++i) {

            var t = i / barCount;
            var x = lerp(t, -100, 100);

            var index = i + h*barCount;
            var id = "box"+index;
            //var box = boxes[index] = builder.createBox(id,[x, -40, z], [1.5,1.5,1.5], [0,1,0,3.1415*0.25], colorMats[i]).attach(testScene);
            var box = boxes[index] = builder.createSphere(id,[x, -40, z], [1.5,1.5,1.5], [0,1,0,3.1415*0.25], '3,2', basicMat).attach(testScene);
        }
    }
   

    window.setInterval( function() {
        detector.update();
        
        var hists = detector.getFrequencyHist();
        //var freqs = detector.getTimeDomain();
        if (hists) {
            for(var h=0; h<hists.length; ++h) {
                var freqs = hists[h];
                for(var i=0; i<barCount; ++i) {
                    var index = i + h*barCount;
                    var mappedIndex = Math.floor(map(i, 0,barCount, 0, freqs.length/4.0));
                    var s = boxes[index].getSize();
                    s[1] = freqs[mappedIndex] * 0.5;
                    boxes[index].setSize(s);
                }
            }
        }
    }, 10);

    return group;
}
