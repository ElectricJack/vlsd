function testBoxWave(root, builder) {
    var group = builder.createNode("group",{
        id: "boxWave"
    });
    root.getRootElement().appendChild(group);

    var rows = 10;
    var cols = 10;
    var boxes = [];
    var s = 10;
    for(var r=0; r<rows; ++r) {
        for(var c=0; c<cols; ++c) {
            var i = c*rows + r;
            var id = "box"+i;
            //var box = builder.createBox(id,[(c-5)*2, (r-5)*2, -10], [1.5,1.5,1.5], [0,0,0,0], basicMat).attach(root);
            var box = builder.createSphere(id,[(c-5)*s, (r-5)*s, -40], [1.2,1.2,1.2], [0,0,0,0], '4,4', basicMat).attach(group);
            boxes[i] = box;
        }
    }
    

    var ang = 0;
    window.setInterval( function() {
        ang += 0.1;
        for(var r=0; r<rows; ++r) {
            for(var c=0; c<cols; ++c) {
                var index = c*rows + r;
                var pos   = boxes[index].getPosition();
                pos[2] = Math.sin(ang + index*0.1) * 5;
                boxes[index].setPosition(pos);
            }
        }
    }, 10);

    return group;
}