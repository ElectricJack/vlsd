

// Global dragon fractal objects
var objects = [];

function startDragon(testScene, builder, startPos) {
    var posOffset   = [ 1, 1, -1];
    var rotationInc = [0,0,0];
    var scaleInc    = [0.98, 0.98, 0.98];

    var group = builder.createNode("group",{
        id: "dragon"
    });
    testScene.getRootElement().appendChild(group);

    dragonIt2(group, builder, 6, startPos, [1.5,1.5,1.5]);

    var ang = 0;
    window.setInterval( function() {
        ang += 0.01;
        for(var i=0; i<objects.length; ++i) {
            var obj = objects[i];
            var objRot = obj.getRotation();
            var newRot = [0.25, 1, 0.25, ang];
            obj.setRotation(newRot);
        }
    }, 10);

    return group;
}


function dragonIt2(parent, builder, numIterations, pos, size) {

    if (numIterations > 0) {
        size = [size[0]*0.9, size[1]*0.9, size[2]*0.9];
        //var obj = builder.createBox("box_"+objects.length, pos, size, [0,0,0,0], basicMat).attach(parent);
        var obj = builder.createSphere("box_"+objects.length, pos, size, [0,0,0,0], '3,2', basicMat).attach(parent);
        objects.push(obj);

        var newPos1 = [-1 * size[0],1 * size[1],1 * size[2]];
        var newPos2 = [ 1 * size[0],-1* size[1],-1 * size[2]];
        dragonIt2(obj, builder, numIterations-1, newPos1, size);
        dragonIt2(obj, builder, numIterations-1, newPos2, size);
    }
}